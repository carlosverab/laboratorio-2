package com.example.cv_laboratorio2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerificar.setOnClickListener {

            if (edtAnio.text.isEmpty()) {
                Toast.makeText(this, "Debes ingresar el año", Toast.LENGTH_SHORT).show()
                imagenGeneracion.setImageBitmap(null)
                tvGeneracion.text = ""
                return@setOnClickListener
            }
            val anio = edtAnio.text.toString().toInt()

            when (anio) {
                in 1930..1948 -> {
                    imagenGeneracion.setImageResource(R.drawable.silentgeneration)
                    tvGeneracion.text = "Generación: Silent Generation Los niños de las postguerra \nRasgo: Austeridad \nPoblación: 6.300.000 \nMarco Temporal: 1930 - 1948"
                }
                in 1949..1968 -> {
                    imagenGeneracion.setImageResource(R.drawable.babyboom)
                    tvGeneracion.text = "Generación: Baby Boom \nRasgo: Ambición \nPoblación: 12.200.000 \nMarco Temporal: 1949 - 1968"
                }
                in 1969..1980 -> {
                    imagenGeneracion.setImageResource(R.drawable.generacionx)
                    tvGeneracion.text = "Generación: Generación X \nRasgo: Obsesión por el éxito \nPoblación: 9.300.000 \nMarco Temporal: 1969 - 1980"
                }
                in 1981..1993 -> {
                    imagenGeneracion.setImageResource(R.drawable.generaciony)
                    tvGeneracion.text = "Generación: Generación Y millennials \nRasgo: Frustración \nPoblación: 7.200.000 \nMarco Temporal: 1981 - 1993"
                }
                in 1994..2010 -> {
                    imagenGeneracion.setImageResource(R.drawable.generacionz)
                    tvGeneracion.text = "Generación: Generación Y millennials \nRasgo: Frustración \nPoblación: 7.200.000 \nMarco Temporal: 1981 - 1993"
                }
                else -> {
                    imagenGeneracion.setImageBitmap(null)
                    tvGeneracion.text = "Generación no definida"

                }
            }

        }
    }
}